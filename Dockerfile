ARG VPN_VERSION=latest

FROM registry.gitlab.com/cabildo-tf/vente/deploy/vpn/dev:${VPN_VERSION}

# hadolint ignore=DL3008
RUN apt-get update && \
    apt-get install --no-install-recommends -y \
		python3-pip \
		curl && \
    rm -rf /var/lib/apt/lists/* && \
    pip3 install --no-cache-dir \
                python-gitlab==2.* \
				requests==2.24.*
COPY src /app

WORKDIR /app

CMD ["/bin/bash", "-c", "/app/sync-gitlab-to-gitea.sh"]