#!/bin/bash

if ! connect-vpn
then
	exit 1
fi

RETRIES=30

echo -e "${INFO_COLOR}Waiting for GITEA ${GITEA_HOST} service is running${NULL_COLOR}"

until [ ${RETRIES} -eq 0 ]
do 
	http_code=$(curl -LI -k ${GITEA_HOST} -o /dev/null -w '%{http_code}\n' -s)
    if [ "${http_code}" == "200" ]
    then
        echo -e "\\n${INFO_COLOR}GITEA service is up${NULL_COLOR}"
        break
    fi

    echo -ne "."

    RETRIES=$((RETRIES-=1))
    sleep 1
done

if [ ${RETRIES} -eq 0 ]
then
    echo -e "\\n${FAIL_COLOR}GITEA connection has failed!${NULL_COLOR}"
    exit 1
fi

python3 /app/sync.py