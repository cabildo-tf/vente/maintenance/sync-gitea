import os
import time
from gitlab import Gitlab
from sync_gitea.repository import Repository
from sync_gitea.gitea import Gitea
import logging


logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("sync.log"),
        logging.StreamHandler()
    ]
)


def sync(gt, gl, gitlab_prefix, gitlab_group_projects, waiting_time_sec=30):
    gt_repositories = gt.get_user_repositories_name()
    gl_groups = gl.groups.list(search=gitlab_group_projects, include_subgroups=True)

    for group in gl_groups:
        projects = group.projects.list()
        for project in projects:
            repo_name = project.name_with_namespace.replace(gitlab_prefix, '').replace(' / ', '-').replace(' ', '_')
            try:
                gt_repositories.remove(repo_name)
                logging.info("El repositorio {} existe".format(repo_name))
            except ValueError as e:
                repository = Repository(repo_name, project.description, project.web_url, project.visibility)
                gt.create_repository(repository)
                logging.info("El repositorio {} no existe".format(repo_name))
                time.sleep(waiting_time_sec)

    return gt_repositories


def delete_gitea_repository(gt, gt_repositories):
    for rep in gt_repositories:
        logging.info("Eliminando repositorio {}".format(rep))
        gt.delete_repository(repo_name=rep)


if __name__ == "__main__":
    gitlab_host = os.getenv('GITLAB_HOST', 'https://gitlab.com')
    gitlab_user = os.getenv('GITLAB_USER')
    gitlab_access_token = os.getenv('GITLAB_ACCESS_TOKEN')
    gitlab_prefix = os.getenv('GITLAB_PREFIX')
    gitlab_group_projects = os.getenv('GITLAB_GROUP_PROJECTS')

    gitea_host = os.getenv('GITEA_HOST', 'https://try.gitea.io')
    gitea_user = os.getenv('GITEA_USER')
    gitea_access_token = os.getenv('GITEA_ACCESS_TOKEN')

    waiting_time_sec = int(os.getenv('WAITING_TIME_SEC', 60))

    if not gitlab_user:
        logging.error("GITLAB_USER is empty")
        exit(os.EX_CONFIG)
    if not gitlab_access_token:
        logging.error("GITLAB_ACCESS_TOKEN is empty")
        exit(os.EX_CONFIG)
    if not gitea_user:
        logging.error("GITEA_USER is empty")
        exit(os.EX_CONFIG)
    if not gitea_access_token:
        logging.error("GITEA_ACCESS_TOKEN is empty")
        exit(os.EX_CONFIG)

    gt = Gitea(host=gitea_host, accesstoken=gitea_access_token, user=gitea_user,
               gitlab_username=gitlab_user, gitlab_accesstoken=gitlab_access_token)
    gl = Gitlab(gitlab_host, private_token=gitlab_access_token)
    gt_repositories = sync(gt, gl, gitlab_prefix=gitlab_prefix, gitlab_group_projects=gitlab_group_projects,
                           waiting_time_sec=waiting_time_sec)
    delete_gitea_repository(gt, gt_repositories=gt_repositories)
