import json

import requests
import urllib3

import logging

urllib3.disable_warnings()


class Gitea(object):
    def __init__(self, **kwargs):
        self.__host = kwargs.pop('host', 'localhost:3000')
        self.__user = kwargs.pop('user', None)
        self.__accesstoken = kwargs.pop('accesstoken', None)
        self.__gitlab_username = kwargs.pop('gitlab_username', None)
        self.__gitlab_accesstoken = kwargs.pop('gitlab_accesstoken', None)
        self.__session = None
        self.__giteaGetUserCache = dict()

    def host(self, end_point):
        return "{0}/api/v1/{1}".format(self.__host, end_point)

    def session(self):
        if self.__accesstoken and not self.__session:
            self.__session = requests.Session()
            self.__session.headers.update({
                "Content-type": "application/json",
                "Authorization": "token {0}".format(self.__accesstoken),
            })
        return self.__session

    def get_user_id(self, username):
        if username in self.__giteaGetUserCache:
            return self.__giteaGetUserCache[username]
        r = self.session().get(self.host('users/{0}'.format(username)), verify=False)
        if r.status_code != 200:
            return 'failed'
        self.__giteaGetUserCache["{0}".format(username)] = json.loads(r.text)["id"]
        return self.__giteaGetUserCache[username]

    def create_repository(self, repository):
        if repository.private:
            repository.set_auth(auth_username=self.__gitlab_username, auth_password=self.__gitlab_accesstoken)
        data = repository.get_data()
        data['uid'] = self.get_user_id(self.__user)

        r = self.session().post(self.host('repos/migrate'), json=data)

        if r.status_code in [200, 201]:
            logging.info("Repository '{0}' created".format(data['repo_name']))
            return 'created'
        elif r.status_code == 409:
            logging.warning("Repository '{0}' already exists".format(data["repo_name"]))
            return 'exists'
        else:
            logging.error("Error creating reporitory '{0}'".format(data["repo_name"]))
            logging.debug(r.status_code, r.text, repository.get_data(secured=True))
            return 'failed'

    def delete_repository(self, repo_name):
        r = self.session().delete(self.host('repos/{0}/{1}'.format(self.__user, repo_name)))

        if r.status_code in [200, 204]:
            logging.info("Repository '{0}' deleted".format(repo_name))
            return 'deleted'
        else:
            logging.error("Error deleting repository '{0}'".format(repo_name))
            logging.debug(r.status_code, r.text)
            return 'failed'

    def get_user_repositories(self):
        loop_count = 1
        results = dict()

        while loop_count != 0:
            url = 'repos/search?uid={0}&page={1}&limit=50'.format(self.get_user_id(self.__user), loop_count)
            r = self.session().get(self.host(url))

            if r.status_code != 200:
                break
            response = json.loads(r.text)
            if response['ok']:
                if len(response['data']) == 0:
                    break
                else:
                    if len(results) == 0:
                        results = response['data']
                    else:
                        results = results + response['data']
            loop_count += 1

        return results

    def get_user_repositories_name(self):
        repositories = self.get_user_repositories()
        return [d['name'] for d in repositories]

