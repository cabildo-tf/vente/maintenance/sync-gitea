class Repository(object):
    def __init__(self, repo_name, description, web_url, visibility):
        self.repo_name = repo_name
        self.description = description
        self.clone_addr = web_url
        self.private = True if visibility == 'private' else False
        self.auth_username = None
        self.auth_password = None
        self.mirror = True

    def set_auth(self, auth_username, auth_password):
        self.auth_username = auth_username
        self.auth_password = auth_password

    def get_data(self, secured=False):
        data = self.__dict__.copy()
        if not self.private:
            for k in ['auth_username', 'auth_password']:
                del data[k]
        elif secured:
            data['auth_password'] = '*' * len(data['auth_password'])
        return data

    def __str__(self):
        return '''
        Name: {repo_name}
        Description: {description}
        URL: {clone_addr}
        Private: {private}'''.format(**self.__dict__)

    def __eq__(self, other):
        properties = ['repo_name', 'description', 'clone_addr', 'private']
        for prop in properties:
            if getattr(self, prop) != getattr(other, prop):
                return False
        return True
